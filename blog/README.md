## Uso

### Añadir post

- ejecutar el script **anhadir_post.py**

    post sin título:

    ```python
    python anhadir_post.py
    ```

    post con título:

    ```python
    python anhadir_post.py -t "Titulo"
    ```

    post con autor:

    ```python
    python anhadir_post.py -a "Autor"
    ```

    post sin título y generar las páginas estáticas:

    ```python
    python anhadir_post.py -g
    ```

    post con título y generar las páginas estáticas:

    ```python
    python anhadir_post.py -gt "Título"
    ```


#### Lo que hace el script es:

    1. añade un post en blog_metadata.yml
    
    2. crea un archivo para el post en formato markdown en la carpeta posts, nombrándolo como [año][mes][dia]-[id].md

si se usa el argumento -g se genera el blog actualizando los post 
(el post nuevo estará sin contenido, habrá que editar su archivo .md y 
regenerar nuevamente las páginas estáticas).


### Actualizar blog 

(regenera las páginas estáticas)

1. ejecutar el script **generate_static_blog.py**

2. actualizar git y subir a repositorio los cambios


---

## Tecnología

Este blog emplea las siguientes tecnologías:

- **Bitbucket**: servicio de alojamiento de proyectos (https://bitbucket.org).

- **Git**: CVS controlador de versiones.

- **Markdown**: lenguaje de marcado. Los posts se escriben en formato .md,
que el módulo markdown de Python transforma a html.

- **Yaml**: formato de serialización de datos para almacenar los datos 
generales del blog y los metadatos de los posts usando el módulo yaml 
de Python.

- **Jinja2**: sistema de plantillas web para generar las páginas html.

- **Python**: lenguaje para leer, modificar y guardar los metadatos
del blog (yaml) y generar las páginas a partir de las plantillas html 
(jinja2).

- **Bootstrap 4**: framework para el front-end de la web (html-css).
