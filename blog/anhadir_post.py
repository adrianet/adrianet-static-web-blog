#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''\
-----------------
TODO
? sort yaml dump

-----------------

Uso:

Añadir un post nuevo con título por defecto "Sin título":
python anhadir_post.py

Añadir un post definiendo el título:
python anhadir_post.py -t [post title]

Añadir un post definiendo el autor:
python anhadir_post.py -a [post author]

Añadir el post con título por defecto "Sin titulo" y generar las páginas estáticas:
python anhadir_post.py -g

Añadir un post nuevo definiendo el título y generar el blog:
python anhadir_post.py -gt "Título post nuevo"

--------------------------
Author: Adrián Eirís
mail: adrianet82@gmail.com
date: 161206
--------------------------
'''


import os
import datetime
import argparse 
import yaml


DATA_FILE = 'blog_metadata.yaml'
POSTS_DIR = 'posts'
DEFAULT_TITLE = 'Sin título'
DEFAULT_AUTHOR = 'Adrián'


def read_data():
    with open(DATA_FILE, 'r') as datafile:
        data = yaml.safe_load(datafile)
    
    return data

def get_date():
    today = datetime.date.today()
    day = today.strftime('%d')
    month = today.strftime('%m')
    year = today.strftime('%Y')
    return [year, month, day]

def add_post(data, title, author):
    date = get_date()
    posts = data['blog']['posts']
    post_id = len(posts) + 1
    post = {'date': {'year': date[0], 'month': date[1], 'day': date[2] },
            'id': post_id, 'title': title, 'author': author}
    
    posts.append(post)
    
    md_filename = '%s%s%s-%s.md' % (date[0], date[1], date[2], post_id)
    open('%s/%s' % (POSTS_DIR, md_filename), 'a').close()

def dump_data(data):
    stream = file(DATA_FILE, 'w')
    yaml.safe_dump(data, stream, allow_unicode = True, encoding = 'utf-8')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--title", metavar='Title', help="Title of the post.")
    parser.add_argument("-a", "--author", metavar='Author', help="Author of the post.")
    parser.add_argument("-g", action='store_true', help="Generate static blog.")
    args = parser.parse_args()
    title = args.title
    author = args.author

    if not title:
        title = DEFAULT_TITLE
        
    if not author:
        author = DEFAULT_AUTHOR
        
    data = read_data()
    add_post(data, title, author)
    dump_data(data)
    
    print '%s: "%s"' % ("Added new post", title)
    
    if args.g:
        import generate_static_blog


if __name__ == "__main__":
    main()
