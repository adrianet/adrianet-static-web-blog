#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''\
--------------------------
Author: Adrián Eirís
mail: adrianet82@gmail.com
date: 161206
--------------------------
'''


try:
    from jinja2 import Environment, FileSystemLoader
except:
    print "jinja2 templates engine module required"
    
try:
    import markdown as md
except:
    print "markdown module required"

import os    
import yaml


BASE_DIR = '.'
DATA_FILE = 'blog_metadata.yaml'
POSTS_DIR = 'posts'
TEMPLATES_DIR = 'templates'
INDEX_TEMPLATE = 'index.html'
POST_TEMPLATE = 'post.html'


def read_data():
    with open(DATA_FILE, 'r') as datafile:
        data = yaml.safe_load(datafile)
    
    return data

def parse_md(mdfilename):
    with open(mdfilename, 'r') as mdfile:
        mdparsed = md.markdown(mdfile.read().decode('utf-8'))

    return mdparsed

def create_html(view, content, url):
    env = Environment(loader=FileSystemLoader(TEMPLATES_DIR))
    env.globals['url'] = url[1]
    template = env.get_template(view)
    return template.render(content)

def create_static_file(html, name, dst):
    with open("%s/%s" % (dst, name), 'w') as static:
        static.write(html.encode("utf-8"))

def main():
    base = read_data()
    site_url = base['blog']['site_url']
    print site_url
    
    #~ Index
    indexname = 'index'
    html_indexname = '%s.html' % (indexname)
    index_url = '%s/%s' % (site_url, html_indexname)
    html = create_html(INDEX_TEMPLATE, base['blog'], [indexname, index_url])
    create_static_file(html, html_indexname, BASE_DIR)
    
    #~ Posts
    posts = base['blog']['posts']
    for post in posts:
        date = post['date']
        postname = '%s%s%s-%s' % (date['year'], date['month'],
                                  date['day'], post['id'])
        mdfilepath = '%s/%s.md' % (POSTS_DIR, postname)
        
        if os.path.exists(mdfilepath):
            post_content = parse_md(mdfilepath)
            post['content'] = post_content 
        
        html_postname = '%s.html' % (postname)
        post_url = '%s/%s' % (site_url, html_postname)
        html = create_html(POST_TEMPLATE, post, [postname, post_url])
        create_static_file(html, html_postname, BASE_DIR)

    print "Static files generated"


main()
